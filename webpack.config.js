/*此文件使用的是commonjs 基于nodejs平台运行*/


//resolve用来拼接绝对路径的方法
const {resolve}=require('path');
const HtmlWebpackPlugin=require('html-webpack-plugin');
module.exports={
    //入口起点
    entry:'./src/index.js',
    //输出
    output:{
        //输出文件名
        //filename:'built.js',
        //输出路径
        //__dirname nodejs的变量，代表当前文件的目录绝对路径
        path:resolve(__dirname,'build')
    },
    module:{ //loader的配置 //loader : 1、下载 2、引用（配置loader）
        rules:[
            {//css编译
                //匹配那些文件
                test: /\.css$/,
                //使用那些loader处理匹配的文件
                use:[//use数组中loader执行顺序：从右到左（从下到上） 依次执行
                    //创建style标签，将js中的样式资源插入进行，添加到head中生效
                    'style-loader',
                    //将css文件编程commonjs模块加载到js中，里面的内容是样式字符串
                    'css-loader'
                ]
            },
            {//less编译
                test:/\.less/,
                use:[
                    'style-loader',
                    'css-loader',
                    'less-loader'//将less文件编译成css文件，需要下载 less-loader 和 less
                ]
            },
            {
                test:/\.(jpg|png|gif|jpeg)$/,
                loader:'url-loader',//下载 url-loader file-loader
                options:{
                    //图片小于8kb，就会按base64处理
                    limit:8*1024
                },
                //使用commonjs解析
                //esModule:false
            },
            {
                test:/\.html$/,
                //处理html文件的img图片(负责引入img，从而能呗url-loader进行处理)
                loader:'html-loader'
            },
            //打包其他资源(除了html/js/css资源以外的资源)
            {
                //排除css|js|html
                //exclude:/\.(css|js|html)/,
                //loader:'file-loader'
            }
        ]
    },
    plugins:[//plugins：1、下载 2、引入 3、使用
        //默认会创建一个空HTML，自动引入打包输入的所有资源（JS/CSS）
        new HtmlWebpackPlugin({
            //复制html文件，并自动引入打包输入的所有资源（JS/CSS）
            template:'./src/index.html'
        })
    ],
    //模式
    //mode:'production'//生产模式（输出文件被压缩）
    mode:'development',//开发模式

    /*开发服务器 devServer ：用来自动化（自动编译，自动打开浏览器，自动刷新浏览器） 
    特点：只会在内存中编译打包，不会有任何输出
    启动devServer指令为：webpack-dev-server
    npx webpack-dev-server //本地启动
    npx webpack serve //解决webpack5下使用webpack-dev-server报错
    */
    devServer:{
        contentBase:resolve(__dirname,'build'),
        //启动gzip压缩
        compress:true,
        //端口号
        port:3000,
        //自动打开浏览器
        //open:true
    }
    
}